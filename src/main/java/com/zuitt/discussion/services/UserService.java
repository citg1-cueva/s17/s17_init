package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Optional;

public interface UserService {

    Optional<User> findByUsername(String username);
    void createUser(User user);

    ArrayList<User> getallUsers();

    Optional<User> getUser(long id);

    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id, User updated);

}
