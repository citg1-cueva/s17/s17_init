package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtToken jwtToken;

    public void createCourse(String stringToken, Course course){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    public Iterable<Course> getCourses() {
        return courseRepository.findAll();
    }



    public ResponseEntity deleteCourse(Long id, String stringToken) {
        Course courseForDeleting = courseRepository.findById(id).get();
        String postAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUserName.equals(postAuthorName)) {
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course deleted successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }
    }


    public ResponseEntity updateCourse(Long id, Course updated,String stringToken) {
        Course courseForDeleting = courseRepository.findById(id).get();
//        getting the author of the specific post
        String postAuthorName = courseForDeleting.getUser().getUsername();
//        get the username from the token to compare it with the postAuthorName
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

//        check if the postAuthorName matches with authenticatedUserName
        if (authenticatedUserName.equals(postAuthorName)) {
            courseForDeleting.setName(updated.getName());
            courseForDeleting.setPrice(updated.getPrice());
            courseForDeleting.setDescription(updated.getDescription());
            courseRepository.save(courseForDeleting);
            return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this course.", HttpStatus.UNAUTHORIZED);
        }
    }


    public Iterable<Course> getMyCourses(String stringToken) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourses();
    }


}


