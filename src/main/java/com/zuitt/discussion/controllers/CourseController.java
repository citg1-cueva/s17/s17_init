package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.services.CourseService;
import com.zuitt.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseServices;

    @RequestMapping(value ="/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(
            @RequestHeader(value = "Authorization")String stringToken,
            @RequestBody Course course){
        courseServices.createCourse(stringToken,course);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses(){
        return new ResponseEntity<>(courseServices.getCourses(), HttpStatus.OK);
    }


    @RequestMapping(value ="/courses/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCourses(
            @PathVariable Long id,
            @RequestHeader(value = "Authorization") String stringToken){
        return courseServices.deleteCourse(id, stringToken);
    }
    @RequestMapping(value ="/courses/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateCourses(
            @PathVariable Long id,
            @RequestHeader(value = "Authorization") String stringToken,
            @RequestBody Course course){
        return courseServices.updateCourse(id, course ,stringToken);
    }
    @RequestMapping(value = "/myCourses", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyCourses(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(courseServices.getMyCourses(stringToken), HttpStatus.OK);
    }


}
